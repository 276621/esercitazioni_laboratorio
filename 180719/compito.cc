#include <iostream>
#include <fstream>

using namespace std;

#include "grafi.h"
#include "coda.h"
#include "node.h"

int ARC_WIEGHT = 1;

void stampa(graph g, node* n)
{
    int dim = get_dim(g);
    char tipo;

    for(int i = 0; i < dim; i++)
    {
        char type = n[i+1].tipo;

        adj_node* p = g.nodes[i];
        while (p != NULL)
        {

            cout << n[i].cont;
            
            if (n[i].tipo == 'U' && n[p->node].tipo == 'T')
            {
                cout << " LIKE ";
            }
            else if(n[i].tipo == 'U' && n[p->node].tipo == 'U')
            {
                cout << " FOLLOW ";
            }
            else if(n[i].tipo == 'T' && n[p->node].tipo == 'U')
            {
                cout << " OWNER ";
            }

            cout << n[p->node].cont << endl;
            p = p->next;
        }

        cout << endl;

    }
}

int* totalLike(graph g, node* n)
{
    int dim = get_dim(g);
    int* likes = new int[dim];

    for (int i = 0; i < dim; i++)
    {
        likes[i] = 0;
    }

    for(int i = 0; i < dim; i++)
    {
        adj_node* p = g.nodes[i];
        adj_node* o = NULL;
        while (p != NULL)
        {
            if (n[i].tipo == 'U' && n[p->node].tipo == 'T')
            {
                o = g.nodes[p->node];
                while (o != NULL && n[o->node].tipo != 'U')
                {
                    o = o->next;
                }
                likes[o->node]++;
            }
            
            p = p->next;
        }
    }
    return likes;
}

int main()
{
    int n_nodi;
    int start_node, arrive_node;
    char buff;
    
    ifstream file_graph("graph");
    if (!file_graph)
    {
        cerr << "Errore nell'apertura del file!";
        return -1;
    }

    file_graph >> n_nodi;
    graph grafo = new_graph(n_nodi);
    
    while (!file_graph.eof())
    {
        file_graph >> start_node;
        file_graph >> arrive_node;
        add_arc(grafo,start_node,arrive_node,ARC_WIEGHT);
    }

    file_graph.close();

    
    ifstream file_nodes("node");
    if(!file_nodes)
    {
        cerr << "Errore nell'apertura del file!";
        return -2;
    }

    node* info_nodi = new node[n_nodi-1];

    for (int i = 0; i < n_nodi; i++)
    {
        file_nodes.getline(info_nodi[i].cont,81);
        file_nodes >> info_nodi[i].tipo;
        file_nodes.ignore();
    }

    cout << "Il grafo contenuto nel file \"graph\" è il seguente: " << endl << endl;

    stampa(grafo, info_nodi);

    int max_like = 0;
    int* tot_like = totalLike(grafo,info_nodi);

    for(int i = 0; i < n_nodi; i++)
    {
        if(tot_like[i] > max_like)
            max_like = tot_like[i];
    }

    cout << "L\'utente con il numero massimo di like è " << endl;

    for(int i = 0; i < n_nodi; i++)
    {
        if(tot_like[i] == max_like)
            cout << info_nodi[i].cont << endl; 
    }

    return 0;
}





