#include <iostream>
#include "tipo.h"
#include "liste.h"
using namespace std;

void pesca(lista& l, istream& input)
{
    tipo_inf new_start;
    
    do
    {
        cout << "Inserisci il valore della carta da pescare: ";
        input >> new_start.valore;
    } while (new_start.valore > 13 || new_start.valore < 1);

    do
    {
        cout << "Inserisci il seme della carta da pescare (C, F, Q, P): ";
        input >> new_start.seme;
    } while (new_start.seme != 'C' && new_start.seme != 'F' && new_start.seme != 'Q' && new_start.seme != 'P');

    
    elem* nuova = new_elem(new_start);
    l = ord_insert_elem(l,nuova);
}

void stampa(lista l)
{
    while (l != NULL)
    {
        print(l->inf);
        l = l->pun;
    }

    cout << endl;
}

elem* scala(lista carte, int& lunghezza)
{
    lunghezza = 0;
    int temp_lun = 0;
    elem* start = NULL;
    elem* start2 = NULL;
    elem* last = carte;
    elem* act = carte->pun;

    while (act != NULL)
    {
        temp_lun = 0;
        start = last;
        while (act != NULL && (act->inf.valore == last->inf.valore + 1) && act->inf.seme == last->inf.seme)
        {
            temp_lun++;
            last = act;
            act = act->pun;
        }

        if (temp_lun > lunghezza)
        {
            lunghezza = temp_lun;
            start2 = start;
        }

        if (act!=NULL)
        {
            last = act;
            act = act->pun;
        }
    }

    if (lunghezza == 0)
        start = NULL;
    else
        start = start2;
    
    return start;
}

int main()
{
    int n_starts;
    lista g1 = NULL;
    lista g2 = NULL;

    cout << "Inserire il numero di carte da avere in mano: ";
    cin >> n_starts;

    cout << endl << "GIOCATORE 1" << endl;

    for (int i = 0; i < n_starts; i++)
    {
        pesca(g1,cin);
    }

    cout << endl << "GIOCATORE 2" << endl;

    for (int i = 0; i < n_starts; i++)
    {
        pesca(g2,cin);
    }
   
   cout << endl;

   
    cout << "Carte GIOCATORE 1: ";
    stampa(g1);
    cout << "Carte GIOCATORE 2: ";  
    stampa(g2);

    int lunghezza_scala = 0;

    elem* start_scala = scala(g1,lunghezza_scala);
    cout << "La scala del GIOCATORE 1: ";
    for (int i = 0; i < lunghezza_scala+1; i++)
    {
        print(start_scala->inf);
        start_scala = start_scala->pun;
    }
    cout << endl;
    

    start_scala = scala(g2,lunghezza_scala);
    cout << "La scala del GIOCATORE 1: ";
    for (int i = 0; i < lunghezza_scala+1; i++)
    {
        print(start_scala->inf);
        start_scala = start_scala->pun;
    }
    cout << endl;


    return 0;
}
