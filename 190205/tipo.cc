#include <iostream>
#include <cstring>
#include "tipo.h"
using namespace std;

//Ordine semi: C,F,P,Q
int compare(tipo_inf c1,tipo_inf c2)
{
    if(c1.seme == c2.seme)
        return c1.valore - c2.valore;
    else
        return c1.seme - c2.seme;
}

void copy(tipo_inf& dc,tipo_inf sc)
{
    dc.seme = sc.seme;
    dc.valore = sc.valore;
}

void print(tipo_inf c)
{
    cout << c.valore << c.seme << " ";
}
