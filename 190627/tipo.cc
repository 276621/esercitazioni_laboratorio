#include <iostream>
#include <cstring>
#include "tipo.h"
using namespace std;

int compare(tipo_inf s,tipo_inf d)
{
    return (s.id-d.id);
}
void copy(tipo_inf &d,tipo_inf s)
{
    d.id = s.id;
    strcpy(d.nome,s.nome);
    d.tipo = s.tipo;
}
void print(tipo_inf l)
{
    cout << l.id << " " << l.nome << " " << l.tipo << endl;
}