#include <iostream>
#include <fstream>
#include <string.h>
#include "tipo.h"
#include "liste.h"
#include "grafo.h"

using namespace std;

const char NOME_FILE_LISTA[] = "PI.txt";
const char NOME_FILE_GRAFO[] = "G.txt";


int carica(lista &l)
{
    int new_id;
    int n_pi = 0;

    ifstream in_file(NOME_FILE_LISTA);

    if (!in_file)
    {
        cerr << "Errore nell\'apertura del file!" << endl;
        exit(1);
    }

    while (in_file >> new_id)
    {
        elem *new_elem = new elem;

        new_elem->inf.id = new_id;
        in_file.ignore();
        in_file.getline(new_elem->inf.nome, 21);
        in_file >> new_elem->inf.tipo;

        l = ord_insert_elem(l, new_elem);
        n_pi++;
    }

    return n_pi;
}

tipo_inf search_pi(lista pi, int id)
{
    while(pi!=NULL)
    {
        if (pi->inf.id == id)
            return pi->inf;
        
        pi = pi->pun;
    }
}

graph mappa(int n)
{
    graph g = new_graph(n);

    int src_node,dst_node;

    ifstream graph_file(NOME_FILE_GRAFO);
    if (!graph_file)
    {
        cerr << "Errore nell\'apertura del file!" << endl;
        exit(2);
    }

    for (int i = 0; i < n; i++)
    {
        graph_file >> src_node;
        graph_file >> dst_node;

        add_edge(g,src_node,dst_node,1);
    }
    
    return g;
}

void stampa_mappa(graph g, lista pi)
{
    cout << "MAPPA DEI PUNTI DI INTERESSE" << endl << endl;

    int dim = get_dim(g);

    for (int i = 0; i < dim; i++)
    {
        adj_node* p = g.nodes[i]->next;

        while (p!=NULL)
        {
            cout << search_pi(pi,g.nodes[i]->node).nome << " ---> " << search_pi(pi,p->node).nome << endl;
            p = p->next;
        }
    }
}

int main()
{

    lista pi = NULL;
    graph g;
    int id;
    int n_pi;

    n_pi = carica(pi);

    cout << "Sono stati caricati " << n_pi << " punti di interesse." << endl;

    cout << "Inserisci l\'ID da cercare: ";
    cin >> id;

    cout << endl << "Il punto di interesse con identificativo " << id <<" è " << search_pi(pi,id).nome << endl; 

    g = mappa(n_pi);

    stampa_mappa(g,pi);

    return 0;
}