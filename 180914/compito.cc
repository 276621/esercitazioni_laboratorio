#include <iostream>
#include <fstream>
#include <cstring>
#include "liste.h"
#include "parola.h"
using namespace std;

parola *load(int &dim)
{
    ifstream inv_file("inverted");
    if (!inv_file)
    {
        cout << "Errore nell'apertura del file.";
        return NULL;
    }

    lista docs;
    int n_words;
    inv_file >> n_words;
    dim = n_words;
    int id_doc;

    parola *word_arr = new parola[n_words];

    for (int i = 0; i < n_words; i++)
    {
        inv_file >> word_arr[i].p;
        inv_file >> word_arr[i].n_doc;
        for (int j = 0; j < word_arr[i].n_doc; j++)
        {
            inv_file >> id_doc;
            elem *nuovo = new_elem(id_doc);
            docs = insert_elem(docs, nuovo);
        }
        word_arr[i].l = docs;
        docs = NULL;
    }
    return word_arr;
}

void stampa(parola *p, int dim)
{
    elem *pointer = NULL;
    for (int i = 0; i < dim; i++)
    {
        cout << p[i].p << endl;
        cout << p[i].n_doc << " documenti" << endl;
        pointer = p[i].l;
        while (pointer->pun != NULL)
        {
            pointer = pointer->pun;
        }

        while (pointer != NULL)
        {
            cout << pointer->inf << " ";
            pointer = pointer->prev;
        }

        cout << endl;
    }
}

void update(parola *&II, char *fileName, int& dim)
{
    ifstream doc_file(fileName);
    if (!doc_file)
    {
        cout << "Errore nel caricamento del file di aggiornamento." << endl;
    }

    tipo_inf id_doc;
    char new_word[80];

    doc_file >> id_doc;
    doc_file >> new_word;

    elem *pointer = NULL;
    elem *nuovo_doc = NULL;
    bool trovato = false;

    while (!doc_file.eof())
    {
        doc_file >> new_word;
        for (int i = 0; i < dim; i++)
        {
            if (strcmp(II[i].p, new_word) == 0)
            {
                pointer = II[i].l;
                while (pointer->pun != NULL)
                {
                    if (id_doc == pointer->inf)
                        trovato = true;

                    pointer = pointer->pun;
                }
                if (!trovato)
                {
                    nuovo_doc = new_elem(id_doc);
                    II[i].l = insert_elem(II[i].l, nuovo_doc);
                    II[i].n_doc++;
                }
            }
            else
            {
                int vecchia_dim = dim;
                dim++;
                parola* new_arr = new parola[dim];

                for (int i = 0; i < vecchia_dim; i++)
                {
                    new_arr[i] = II[i];
                }
                new_arr[dim-1].l->inf = id_doc;
                new_arr[dim-1].n_doc = 1;
                strcpy(new_arr[dim-1].p,new_word);
            }
        }
    }
}

int main()
{
    int dim = 0;
    parola *words_arr = load(dim);
    stampa(words_arr, dim);

    char update_fileName[50];

    cout << "Inserire il nome del file di aggiornamento: ";
    cin >> update_fileName;

    update(words_arr, update_fileName, dim);

    stampa(words_arr, dim);

    return 0;
}
