/**
 * @mainpage 
 * @file compito.cc
 * @author francesco caligiuri
 * @date 2022-07-07 
 */

#include <iostream>
#include <cstring>
using namespace std;

#include "carta.h"
#include "bst.h"

int totale_punti(bst b, int inf, int sup)
{
    int totale = 0;
    if(b != NULL)
    {
        if ((b->key > inf) && (b->key < sup))
            totale += b->inf.punti;

        return totale + totale_punti(b->left,inf,sup) + totale_punti(b->right,inf,sup);
    }
    return 0;
}

void aggiorna(bst &b, tipo_key id, int punti_aggiunti)
{
    bnode* p = b;
    bnode* new_n;
    tipo_inf updated_inf;
    p = bst_search(b, id);

    if(p == NULL)
    {
        cout << endl << "Errore! ID non esistente." << endl << endl;
    }
    else if(p != NULL)
    {
        strcpy(updated_inf.nome_cognome, p->inf.nome_cognome);
        updated_inf.punti = p->inf.punti + punti_aggiunti;
        copy(p->inf,updated_inf);

        cout << endl << "Aggiornamento riuscito!" << endl << endl;

    }
}

int main()
{
    bst card_tree = NULL;
    int n_carte = 0;
    int n_acquisti = 0;
    int punti;

    cout << "Inserire il numero di carte fedeltà da inserire: ";
    cin >> n_carte;

    tipo_key id;
    tipo_inf info;
    //char nome[40];
    //int punti;

    for (int i = 0; i < n_carte; i++)
    {
        cout << "Carta " << i + 1 << " - Inserire ID carta: ";
        cin >> id;

        cin.ignore();

        cout << "Carta " << i + 1 << " - Inserire Nome e Cognome del titolare della carta: ";
        cin.getline(info.nome_cognome,40);

        cout << "Carta " << i + 1 << " - Inserire punti fedeltà presenti nella carta: ";
        cin >> info.punti;

        bnode *nuova_carta = bst_newNode(id, info);  
        bst_insert(card_tree, nuova_carta);

        cout << endl;
    }

    print_bst(card_tree);

    cout << "Il totale dei punti delle carte che hanno ID da 2000 a 4000 è: " << totale_punti(card_tree, 2000, 4000) << endl;

    cout << "Inserire numero di acquisti effettuati: " << endl;
    cin >> n_acquisti;

    for (int i = 0; i < n_acquisti; i++)
    {
        cout << "Inserire ID carta e punti da aggiungere, separati da uno spazio: ";
        cin >> id;
        cin >> punti;

        aggiorna(card_tree,id,punti);
    }

    print_bst(card_tree);

    return 0;
}
