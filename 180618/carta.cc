#include <iostream>
#include <string.h>
using namespace std;

#include "carta.h"

int compare(tipo_inf c1, tipo_inf c2)
{
    return strcmp(c1.nome_cognome,c2.nome_cognome);
}

void copy(tipo_inf& c1,tipo_inf c2)
{
    c1.punti = c2.punti;
    strcpy(c1.nome_cognome, c2.nome_cognome);
}

void print(tipo_inf c)
{
    cout << c.nome_cognome << endl;
    cout << c.punti;
}
